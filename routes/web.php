<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');
Route::get('/dashboard',  [DashboardController::class, 'index'])->name('dashboard');

Route::resource('app-admins', AdminController::class);
Route::resource('app-users', UserController::class);
Route::resource('projects', ProjectController::class);
Route::get('users', [AdminController::class, 'getAdminUsers'])->name('users');
Route::get('user', [UserController::class, 'getAdminUsers'])->name('user');

Route::get('project', [ProjectController::class, 'getAdminUsers'])->name('project');

Route::get('my-projects/{id}', [UserController::class, 'MyProjects'])->name('my-projects');
//my project
Route::get('project-user/{id}', [ProjectController::class, 'getUserProject'])->name('project-user');

require __DIR__.'/auth.php';
