<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = User::where('email', '=', 'manager@biz4d.com')->exists();
        if (!$exists){
            $admin = User::create([
                'name'=>'Manager Biz4d',
                'email'=>'manager@biz4d.com',
                'phone'=>'0745521920',
                'password'=>Hash::make('secretpassword'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
//                'status'=>true
            ]);
            $admin->assignRole('manager');
        }
    }
}
