<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('statuses')->insert([
            ['name' => 'Active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Incomplete',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Ongoing ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Stuck',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],

        ]);
    }
}
