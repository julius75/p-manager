<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = User::where('email', '=', 'admin@biz4d.com')->exists();
        if (!$exists){
            $admin = User::create([
                'name'=>'Admin Biz4d',
                'email'=>'admin@biz4d.com',
                'phone'=>'07446221920',
                'password'=>Hash::make('secretpassword'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
//                'status'=>true
            ]);
            $admin->assignRole('super_admin');
        }
    }
}
