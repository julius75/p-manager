<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', '=', 'super_admin')->exists() and
            !Role::where('name', '=', 'manager')->exists() and
            !Role::where('name', '=', 'user')->exists())
        {
            DB::table('roles')->insert([
                ['name' => 'super_admin',
                    'guard_name' => 'web',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
                ['name' => 'manager',
                    'guard_name' => 'web',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
                ['name' => 'user',
                    'guard_name' => 'web',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
            ]);
        }
    }
}
