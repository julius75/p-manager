@extends('layout.master')
@section('content')
    <div class="row" style="margin-top: -5%;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('projects.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('projects.update',$project->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        {{ method_field('PUT') }})
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name"  value="{{ $project->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <input class="form-control" style="height:80px" value="{{ $project->description }}" name="description" placeholder="Description"/>
                </div>
                <div class="form-group">
                    <strong>Link:</strong>
                    <input type="text" name="link" value="{{ $project->link }}" class="form-control" placeholder="Link">
                </div>
                <div class="form-group">
                    <strong>Status:</strong>
                    <select name="status_id" class="form-control">
                        <option selected disabled value="" >-- Select Status --</option>
                        <!-- Read Departments -->
                        @foreach($status as $main)
                            <option value='{{ $main->id }}'>{{ $main->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Image:</strong>
                    <input type="file" name="image" value="{{ $project->image }}" class="form-control" placeholder="image">
                </div>
                <div class="form-group">
                    <strong>Document:</strong>
                    <input type="file" name="document" value="{{ $project->document }}" class="form-control" placeholder="Document">
                </div>
                <div class="form-group">
                    <strong>Video:</strong>
                    <input type="file" name="video" value="{{ $project->video }}"class="form-control" placeholder="Video">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
@section('scripts')

@endsection

