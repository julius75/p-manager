<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use PharIo\Manifest\Author;
use Yajra\DataTables\Facades\DataTables;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.index');
    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminUsers()
    {
        $user = Auth::user();
        if ($user->hasRole(['user'])) {
            $users = Project::where('user_id',$user->id)->get();
            return Datatables::of($users)
                ->addColumn('user', function ($users){
                    return $users->user->name;
                })
                ->addColumn('action', function ($users) {
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.show',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Project</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.edit',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit Project</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
                })
                ->rawColumns(['user', 'action'])
                ->make(true);
        }
        else{

        $users = Project::all();
        //  return $users->roles->first()->name;
        return Datatables::of($users)
            ->addColumn('user', function ($users){
                return $users->user->name;
            })
            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.show',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Project</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.edit',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit Project</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.destroy',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete Project</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
            })
            ->rawColumns(['user', 'action'])
            ->make(true);
    }
    }
    public function getUserProject($id)
    {
       // $users =  User::with('roles')->get();
        $users = Project::where('user_id',$id)->get();
        //  return $users->roles->first()->name;
        return Datatables::of($users)
            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.show',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Project</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('projects.edit',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit Project</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
            })
            ->rawColumns(['user', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::query()->orderby("name","asc")->select('id','name')->get();
        return view('projects.create',compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'video' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'document' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        if ($image = $request->file('document')) {
            $destinationPath = 'document/';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['document'] = "$profileImage";
        }
        $input['user_id'] = Auth::user()->id;
        Project::create($input);
        return redirect()->route('projects.index')
            ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::query()->orderby("name","asc")->select('id','name')->get();

        $project = Project::findOrFail($id);

        return view('projects.edit',compact('project','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'video' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'document' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        else{
            unset($input['image']);
        }
        if ($image = $request->file('document')) {
            $destinationPath = 'document/';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['document'] = "$profileImage";
        }
        else{
            unset($input['image']);
        }

        $project = Project::query()->findOrFail($id);
        $input['user_id'] = Auth::user()->id;
        $project->update($input);
        return redirect()->route('projects.index')
            ->with('success','Project Updated created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $project = Project::where('id', '=', $id)->firstOrFail();
        $project->delete();
        return redirect()->route('projects.index')
            ->with('success','Project deleted successfully');
    }
}
