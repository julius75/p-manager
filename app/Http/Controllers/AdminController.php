<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index-admin-user');

    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminUsers()
    {
        $user = Auth::user();
        if ($user->hasRole(['manager'])) {
            $users = User::role('manager')->get();
            return Datatables::of($users)
                ->addColumn('role', function ($users){
                    if ($users->roles->first()->name == 'manager'){
                        return "Manager";
                    }
                })
                ->addColumn('action', function ($users) {
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('app-admins.show',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('app-admins.edit',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
                })
                ->rawColumns(['role', 'action'])
                ->make(true);
        }
        else{
        $users = User::role('manager')->get();
        //  return $users->roles->first()->name;
        return Datatables::of($users)
            ->addColumn('role', function ($users){
                if ($users->roles->first()->name == 'manager'){
                    return "Manager";
                }
            })
            ->addColumn('action', function ($users) {
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('app-admins.show',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('app-admins.edit',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('app-admins.destroy',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
                })
            ->rawColumns(['role', 'action'])
            ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $role = $user->roles->first()->name;
        return view('users.show-admin',compact('user','role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role = $user->roles->first()->name;
        return view('users.edit-admin-user',compact('user','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
