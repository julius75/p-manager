<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'description',
        'image',
        'video',
        'document',
        'user_id',
        'link',
        'status_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
